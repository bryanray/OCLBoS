# OpenCL Fuse Tutorials #

This project contains learning materials for OpenCL Fuse development for Blackmagic Fusion. 

Discussion and descriptions are available at pages indexed from: [http://www.bryanray.name/wordpress/opencl-fuses-index/](http://www.bryanray.name/wordpress/opencl-fuses-index/)

At present, these fuses have only been tested in Fusion 8.2.1 build 6 on Windows 7 with NVidia GPUs. Other environments may require adjustment.